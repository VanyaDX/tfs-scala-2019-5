package practice

import java.io.Closeable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.{BufferedSource, Source}

case class Bucket(min: Int, max: Int, count: Int)

case class Stats(lineCount: Int,
                 minLineLength: Int,
                 averageLineLength: Int,
                 maxLineLength: Int,
                 buckets: Seq[Bucket])


object Practice extends App {
  private val bucketSize = 10
  private val defaultFileName = "in.txt"

  private def getSource(fileName: String): BufferedSource =
    Source.fromResource(fileName)
  private def read(in: BufferedSource): Future[Iterator[String]] =
    Future(blocking(in.getLines()))

  private def printStats(stats: Stats): Unit = {
    import stats._
    println(
      s"""
         | Total line count: $lineCount
         | min: $minLineLength
         | avg: $averageLineLength
         | max: $maxLineLength
         |
         | buckets:
         |${
        buckets.map { b =>
          import b._
          s"   - $min-$max: $count"
        }.mkString("\n")
      }""".stripMargin)
  }

  /*
    Методы для разминки
   */

  def asyncWithResource[R <: Closeable, T](resource: R)(block: R => Future[T]): Future[T] =
    block(resource).andThen{ case _ => {
      resource.close()
    } }

  def asyncCountLines: Future[Int] = {
    val f: Future[List[String]] = asyncWithResource(getSource(defaultFileName))(read(_).map(_.toList))
    f.map(_.count(_ => true))
    /*for {
      iter <- f
    } yield iter.count(_ => true)*/
  }

  def asyncLineLengths: Future[Seq[(String, Int)]] = {
    val f: Future[List[String]] = asyncWithResource(getSource(defaultFileName))(read(_).map(_.toList))
    f.map(it => it.map(s => (s, s.length * 20) ) )
    /*for {
      iter <- f
      s <- iter
    } yield (s, s.length)*/
  }

  def asyncTotalLength: Future[Int] = {
    val f = asyncWithResource(getSource(defaultFileName)) { in => read(in) }
    //f.map()
    f.map(_.map(_.length).sum)
    /*val res: Future[Seq[Int]] = for {
      iter <- f
      s <- iter
    } yield s.length
    res.map(_.sum)*/
  }

  def countShorterThan(maxLength: Int): Future[Int] = {
    val f = asyncWithResource(getSource(defaultFileName)) { in => read(in) }
    f.map(_.count(s => s.length < maxLength))
  }


  /*
    Sleep sort
    https://www.quora.com/What-is-sleep-sort
   */

  def printWithDelay(delay: FiniteDuration, s: String) = {
    //println(delay.length + " " + s)
    Thread.sleep(delay.length)
    println(delay.length + " " + s)
  }

  def sleepSort: Future[Unit] = {
    val f: Future[Seq[(String, Int)]] = asyncLineLengths
    //f.map(_.foreach{case (s, i) => printWithDelay(i.seconds, s)})
    f.map(_.map(line =>
    {
      val thread = new Thread {
        override def run {
          printWithDelay(line._2.seconds, line._1)
        }
      }
      thread.start()
      thread
    })
      .foreach(_.join()))
  }

  /*
    Calculate file statistics
   */

  def splitToBuckets(linesWithLengths: Seq[(String, Int)]): Future[Seq[Bucket]] =
    ???

  def calculateStats: Future[Stats] =
    ???

  //Await.result(asyncLineLengths, 10000.seconds).map(println)
  Await.result(sleepSort, 100000.seconds)
  print("Success")
}
