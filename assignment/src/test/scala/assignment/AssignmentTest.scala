package assignment

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.ConfigFactory
import org.scalatest.Matchers._
import org.scalatest._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise, TimeoutException}
import scala.util.Random

class AssignmentTest extends AsyncFlatSpec {

  val config = ConfigFactory.load()
  val credentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt = new AsyncBcryptImpl
  val assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }
  it should "return false if user does not exist in store" in {
    verifyCredentials("random", "pooh").map { result =>
      result shouldBe false
    }
  }
  it should "return false for invalid password" in {
    verifyCredentials("winnie", "wrongPass").map { result =>
      result shouldBe false
    }
  }

  behavior of "withCredentials"

  it should "execute code block if credentials are valid" in {
    @volatile var counter = 0
    withCredentials("winnie", "pooh")(counter += 1).map { _ =>
      counter shouldBe 1
    }
  }
  it should "not execute code block if credentials are not valid" in {
    @volatile var counter = 0
    withCredentials("winnie", "wrongPass")(counter += 1).recover {
      case _ => true
    }.map { _ =>
      counter shouldBe 0
    }
  }

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in {
    val passwords = Seq("qwer", "123", "asdf")
    hashPasswordList(passwords).flatMap { f =>
      Future.sequence(f.map(x => reliableBcrypt.verify(x._1, x._2)))
    }.map { result =>
      result.forall(_ == true) shouldBe true
    }
  }

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in {
    val passwords = Seq("qwer", "123", "asdf")
    val pass = "qwer"
    reliableBcrypt.hash(pass).flatMap { hash =>
      findMatchingPassword(passwords, hash).map { result =>
        result shouldBe Some(pass)
      }
    }
  }
  it should "return None if no matching password is found" in {
    val passwords = Seq("qwer", "123", "asdf")
    val pass = "qwerty"
    reliableBcrypt.hash(pass).flatMap { hash =>
      findMatchingPassword(passwords, hash).map { result =>
        result shouldBe None
      }
    }
  }


  case class ExceptionMy(value: Int) extends Exception

  behavior of "withRetry"

  it should "return result on passed future's success" in {
    @volatile var counter = 0
    withRetry(Future { counter += 1 }, 5).map { _ =>
      counter shouldBe 1
    }
  }
  it should "not execute more than specified number of retries" in {
    @volatile var counter = 0
    withRetry(Future.failed { counter += 1; new Exception }, 5).recover {
      case _ => true
    }.map { _ =>
      counter shouldBe 5
    }
  }
  it should "not execute unnecessary retries" in {
    @volatile var counter = 0
    withRetry(Future { counter += 1 }, 5).map { _ =>
      counter shouldBe 1
    }
  }
  it should "return the first error, if all attempts fail" in {
    @volatile var counter = 0
    withRetry(Future.failed { counter += 1; ExceptionMy(counter) }, 5)
      .recover {
        case exception: ExceptionMy if exception.value == 1 => true
        case _ => false
      }
      .map { result =>
        result shouldBe true
      }
  }

  behavior of "withTimeout"

  it should "return result on passed future success" in {
    withTimeout(Future.successful(true), 5.seconds).map { result =>
      result shouldBe true
    }
  }
  it should "return result on passed future failure" in {
    withTimeout(Future.failed(ExceptionMy(0)), 5.seconds).recover {
      case _: ExceptionMy => true
      case _ => false
    }.map { result =>
      result shouldBe true
    }
  }
  it should "complete on never-completing future" in {
    withTimeout(Promise[Int]().future, 5.seconds).recover {
      case _: TimeoutException => true
      case _ => false
    }.map { result =>
      result shouldBe true
    }
  }

  behavior of "hashPasswordListReliably"
  val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)

  it should "return password-hash pairs for successful hashing operations" in {
    val passwords = Seq.fill(10)("password" + Random.nextInt(100000000))
    assignmentFlaky.hashPasswordListReliably(passwords, 10, 1.millisecond).flatMap { f =>
      Future.sequence(f.map(x => reliableBcrypt.verify(x._1, x._2)))
    }.map { result =>
      result.forall(_ == true) shouldBe true
    }
  }
}
