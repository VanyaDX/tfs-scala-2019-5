package assignment

import java.util.concurrent.TimeoutException

import bcrypt.AsyncBcrypt
import com.typesafe.scalalogging.StrictLogging
import store.AsyncCredentialStore

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

class Assignment(bcrypt: AsyncBcrypt, credentialStore: AsyncCredentialStore)
                (implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
    * проверяет пароль для пользователя
    * возвращает Future со значением false:
    *   - если пользователь не найден
    *   - если пароль не подходит к хешу
    */
  def verifyCredentials(user: String, password: String): Future[Boolean] =
    credentialStore.find(user).flatMap{
      case Some(hash) => bcrypt.verify(password, hash)
      case _ => Future.successful(false)
    }

  /**
    * выполняет блок кода, только если переданы верные учетные данные
    * возвращает Future c ошибкой InvalidCredentialsException, если проверка не пройдена
    */
  def withCredentials[A](user: String, password: String)(block: => A): Future[A] =
    verifyCredentials(user, password).flatMap {
      case true => Future(block)
      case false => Future.failed(new InvalidCredentialsException())
    }

  /**
    * хеширует каждый пароль из списка и возвращает пары пароль-хеш
    */
  def hashPasswordList(passwords: Seq[String]): Future[Seq[(String, String)]] =
    Future.sequence(
      passwords.map(pass => bcrypt.hash(pass).map(pass -> _))
    )

  /**
    * проверяет все пароли из списка против хеша, и если есть подходящий - возвращает его
    * если подходит несколько - возвращается любой
    */
  def findMatchingPassword(passwords: Seq[String], hash: String): Future[Option[String]] =
    Future.sequence(passwords.map(pass => bcrypt.verify(pass, hash).map(pass -> _)))
      .map(_.find(_._2).map(_._1))

  /**
    * логирует начало и окончание выполнения Future, и продолжительность выполнения
    */
  def withLogging[A](tag: String)(f: => Future[A]): Future[A] = {
    val startTime = System.currentTimeMillis()
    logger.info(s"Task: $tag -- Started")
    f.onComplete(_ =>
      logger.info(s"Task: $tag -- Сompleted - Execution time: ${System.currentTimeMillis() - startTime} ms"))
    f
  }

  /**
    * пытается повторно выполнить f retries раз, до первого успеха
    * если все попытки провалены, возвращает первую ошибку
    *
    * Важно: f не должна выполняться большее число раз, чем необходимо
    */
  def withRetry[A](f: => Future[A], retries: Int): Future[A] = {
    require(retries > 0)
    f.recoverWith {
      case ex if retries > 1 => withRetry(f, retries - 1).recover(throw ex)
    }
  }

  /**
    * по истечению таймаута возвращает Future.failed с java.util.concurrent.TimeoutException
    */
  def withTimeout[A](f: Future[A], timeout: FiniteDuration): Future[A] = {
    try
      Await.ready(f, timeout)
    catch {
      case timeoutException: TimeoutException => Future.failed(timeoutException)
    }
  }

  /**
    * делает то же, что и hashPasswordList, но дополнительно:
    *   - каждая попытка хеширования отдельного пароля выполняется с таймаутом
    *   - при ошибке хеширования отдельного пароля, попытка повторяется в пределах retries (свой на каждый пароль)
    *   - возвращаются все успешные результаты
    */
  def hashPasswordListReliably(passwords: Seq[String], retries: Int, timeout: FiniteDuration): Future[Seq[(String, String)]] = {
    Future.sequence(
      passwords.map(pass => withRetry(withTimeout(bcrypt.hash(pass), timeout).map(pass -> _), retries)
        .map(Success(_))
        .recover{case exp => Failure(exp)}
      )
    ).map(_.filter(_.isSuccess).map(_.get))
  }
}
